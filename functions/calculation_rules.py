"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))





# Add new functions for calculating metrics

def kpi_max_delivery_time(system: LegoAssembly)->float:
    """
    Calculates the maximal delivery time of the system. 
    This is the time of the piece that need the longest time until it arrives.

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        [max_time, max_time_item_number, max_time_item_description] (float): [This is the time of the piece that need the longest time until it arrives (in Days), the item number, the item description]

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_max_delivery_time()")

    max_time = 0
    max_time_item_number = "no"
    max_time_item_description = "no"
    for c in system.get_component_list(-1):
        if max_time < c.properties["delivery time [days]"]:
            max_time = c.properties["delivery time [days]"]
            max_time_item_number = c.properties["item number"]
            max_time_item_description = c.properties["item description"]
    return [max_time, max_time_item_number, max_time_item_description]




def kpi_sum_price(system: LegoAssembly)->float:
    """
    Calculates the total price of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_price (float): Sum of prices of all components in the system in Euro

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_sum_price()")

    total_price = 0
    for c in system.get_component_list(-1):
        total_price += c.properties["price [Euro]"]
    return total_price # alternative: sum(c.properties["price [Euro]"] for c in system.get_component_list(-1))





def kpi_part_with_highest_price(system: LegoAssembly)->float:
    """
    Calculates the part with the highest price of the system. 
    
    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        [max_price, max_price_item_number, max_price_item_description](float): [price of the item, item number, item description]
       

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_part_with_highest_price()")

    max_price = 0
    max_price_item_number = "no"
    max_price_item_description = "no"
    for c in system.get_component_list(-1):
        if max_price < c.properties["price [Euro]"]:
            max_price = c.properties["price [Euro]"]
            max_price_item_number = c.properties["item number"]
            max_price_item_description = c.properties["item description"]
    return [max_price, max_price_item_number, max_price_item_description]




#def kpi_mass_per_price(system: LegoAssembly)->float:
    """
    Calculates the mass per price of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        mass_price (float): Mass per prices of all components in the system in g/Euro

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    #if not isinstance(system, LegoAssembly):
     #   raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass_per_price()")
    
    #total_mass = kpi_mass(system: LegoAssembly)
    #total_price = kpi_sum_price(system: LegoAssembly)
    
    #mass_price = total_mass/total_price
    #return mass_price  






if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
